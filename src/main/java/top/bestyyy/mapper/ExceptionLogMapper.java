package top.bestyyy.mapper;

import org.apache.ibatis.annotations.Mapper;
import top.bestyyy.entity.ExceptionLog;

import java.util.List;

/**
 * 异常日志持久层接口
 */
@Mapper
public interface ExceptionLogMapper {
	List<ExceptionLog> getExceptionLogListByDate(String startDate, String endDate);

	int saveExceptionLog(ExceptionLog log);

	int deleteExceptionLogById(Long id);
}
