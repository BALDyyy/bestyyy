package top.bestyyy.mapper;

import org.apache.ibatis.annotations.Mapper;
import top.bestyyy.entity.Friend;

import java.util.List;

/**
 * 友链持久层接口
 */
@Mapper
public interface FriendMapper {
	List<Friend> getFriendList();

	List<top.bestyyy.model.vo.Friend> getFriendVOList();

	int updateFriendPublishedById(Long id, Boolean published);

	int saveFriend(Friend friend);

	int updateFriend(top.bestyyy.model.dto.Friend friend);

	int deleteFriend(Long id);

	int updateViewsByNickname(String nickname);
}
