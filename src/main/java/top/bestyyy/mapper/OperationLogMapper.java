package top.bestyyy.mapper;

import org.apache.ibatis.annotations.Mapper;
import top.bestyyy.entity.OperationLog;

import java.util.List;

/**
 * 操作日志持久层接口
 */
@Mapper
public interface OperationLogMapper {
	List<OperationLog> getOperationLogListByDate(String startDate, String endDate);

	int saveOperationLog(OperationLog log);

	int deleteOperationLogById(Long id);
}
