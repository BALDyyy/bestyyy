package top.bestyyy.mapper;

import org.apache.ibatis.annotations.Mapper;
import top.bestyyy.entity.SiteSetting;

import java.util.List;

/**
 * 站点设置持久层接口
 */
@Mapper
public interface SiteSettingMapper {
	List<SiteSetting> getList();

	List<SiteSetting> getFriendInfo();

	String getWebTitleSuffix();

	int updateSiteSetting(SiteSetting siteSetting);

	int deleteSiteSettingById(Integer id);

	int saveSiteSetting(SiteSetting siteSetting);

	int updateFriendInfoContent(String content);

	int updateFriendInfoCommentEnabled(Boolean commentEnabled);
}
