package top.bestyyy.mapper;

import org.apache.ibatis.annotations.Mapper;
import top.bestyyy.entity.Tag;
import top.bestyyy.model.vo.TagBlogCount;

import java.util.List;

/**
 * 博客标签持久层接口
 */
@Mapper
public interface TagMapper {
	List<Tag> getTagList();

	List<Tag> getTagListNotId();

	List<Tag> getTagListByBlogId(Long blogId);

	int saveTag(Tag tag);

	Tag getTagById(Long id);

	Tag getTagByName(String name);

	int deleteTagById(Long id);

	int updateTag(Tag tag);

	List<TagBlogCount> getTagBlogCount();
}
