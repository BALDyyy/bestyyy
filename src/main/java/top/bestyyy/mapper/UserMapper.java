package top.bestyyy.mapper;

import org.apache.ibatis.annotations.Mapper;
import top.bestyyy.entity.User;

/**
 * 用户持久层接口
 */
@Mapper
public interface UserMapper {
	User findByUsername(String username);
}
