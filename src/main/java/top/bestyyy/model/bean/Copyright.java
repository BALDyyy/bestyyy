package top.bestyyy.model.bean;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * copyright
 */
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Copyright {
	private String title;
	private String siteName;
}
