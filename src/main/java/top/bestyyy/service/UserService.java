package top.bestyyy.service;

import top.bestyyy.entity.User;

public interface UserService {
	User findUserByUsernameAndPassword(String username, String password);
}
