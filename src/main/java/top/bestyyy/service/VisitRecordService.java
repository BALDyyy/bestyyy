package top.bestyyy.service;

import top.bestyyy.entity.VisitRecord;

public interface VisitRecordService {
	void saveVisitRecord(VisitRecord visitRecord);
}
