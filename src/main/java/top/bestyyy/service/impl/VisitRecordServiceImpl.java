package top.bestyyy.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.bestyyy.entity.VisitRecord;
import top.bestyyy.mapper.VisitRecordMapper;
import top.bestyyy.service.VisitRecordService;

/**
 * 访问记录业务层实现
 */
@Service
public class VisitRecordServiceImpl implements VisitRecordService {
	@Autowired
	VisitRecordMapper visitRecordMapper;

	@Override
	public void saveVisitRecord(VisitRecord visitRecord) {
		visitRecordMapper.saveVisitRecord(visitRecord);
	}
}
